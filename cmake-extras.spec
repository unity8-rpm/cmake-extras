Name:		cmake-extras
Version:	r241
Release:	1%{?dist}
Summary:	A collection of add-ons for the CMake build tool

Group:		tools
License:	GPL-3.0
URL:		https://ubports.io
Source0:	https://github.com/abhishek9650/cmake-extras/archive/%{name}-%{version}.tar.gz
Patch1:		Gtest.patch

BuildRequires:	cmake
BuildRequires:  python
Requires:	python



%description
A collection of add-ons for the CMake build tool.

%global debug_package %{nil}

%prep
%setup -q
%patch1 -p1

%build
%cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr .
make %{?_smp_mflags}


%install
make install DESTDIR=%{buildroot}


%files
%{_datadir}/cmake
%{_datadir}/cmake/CopyrightTest
%{_datadir}/cmake/CoverageReport
%{_datadir}/cmake/DoxygenBuilder
%{_datadir}/cmake/FormatCode
%{_datadir}/cmake/GDbus
%{_datadir}/cmake/GMock
%{_datadir}/cmake/GSettings
%{_datadir}/cmake/IncludeChecker
%{_datadir}/cmake/Intltool
%{_datadir}/cmake/Lcov
%{_datadir}/cmake/QmlPlugins
%{_datadir}/cmake/gcovr



%changelog
* Sat Sep 29 2018 Abhishek Mudgal <abhishek.mudgal.59@gmail.com>
- First adaptation
